﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{

    [AlgorithmInfo("Estrazione Contorno", Category = "FEI")]
    [CustomAlgorithmPreviewOutput(typeof(ContourExtractionViewer))]
    public class EstrazioneContorni : TopologyOperation<List<CityBlockContour>>
    {
        public override void Run()
        {
            Result = new List<CityBlockContour>();
            Image<byte> image = new ImageBorderFilling<byte>(this.InputImage, 1, 0, false).Execute();
            ImageCursor pixelStart = new ImageCursor(image, 1);
            Image<bool> visited = new Image<bool>(image.Width, image.Height, false);
            do
            {
                if (!visited[pixelStart] && image[pixelStart] == Foreground &&
                  (image[pixelStart.North] != Foreground
                  || image[pixelStart.West] != Foreground
                  || image[pixelStart.South] != Foreground
                  || image[pixelStart.East] != Foreground))
                {
                    ImageCursor c = new ImageCursor(pixelStart);
                    CityBlockDirection direction = CityBlockDirection.West;
                    bool clockwise = true;
                    for (int j = 1; j <= 4; j++)
                    {
                        direction = CityBlockMetric.GetNextDirection(direction);
                        if (image[c.GetAt(direction)] == Foreground && (image[c.GetAt(CityBlockMetric.GetPreviousDirection(direction))] != Foreground || image[c.GetAt(CityBlockMetric.GetNextDirection(direction))] != Foreground))
                        {
                            clockwise = image[c.GetAt(CityBlockMetric.GetNextDirection(direction))] == Foreground;
                            break;
                        }
                    }
                    CityBlockContour contour = new CityBlockContour(pixelStart.X, pixelStart.Y);
                    do
                    {
                        for (int j = 1; j <= 4; j++)
                        {
                            direction = clockwise ? CityBlockMetric.GetPreviousDirection(direction) : CityBlockMetric.GetNextDirection(direction);
                            if (image[c.GetAt(direction)] == Foreground)
                            {
                                break;
                            }
                        }
                        contour.Add(direction);
                        c.MoveTo(direction);
                        visited[c] = true;
                        direction = CityBlockMetric.GetOppositeDirection(direction);
                    } while (c != pixelStart && image[c] == Foreground);
                    this.Result.Add(contour);
                }
            } while (pixelStart.MoveNext());
        }
    }


}
