﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{
  [AlgorithmInfo("Modulo Gradiente", Category = "FEI")]
  public class CalcolaModuloGradiente : ImageOperation<Image<byte>, Image<int>>
  {
    private Image<int> deltaXImage { get; set; }
    private Image<int> deltaYImage { get; set; }
    public override void Run()
    {
      ImageCursor c = new ImageCursor(this.InputImage, 1);
      this.Result = new Image<int>(this.InputImage.Width, this.InputImage.Height, 0);
      this.deltaXImage = this.Result.Clone();
      this.deltaYImage = this.Result.Clone();
      do
      {
        int Dx = ((this.InputImage[c.Northeast] + this.InputImage[c.East] + this.InputImage[c.Southeast]) - (this.InputImage[c.Northwest] + this.InputImage[c.West] + this.InputImage[c.Southwest])) / 3;
        this.deltaXImage[c] = Dx;
        int Dy = ((this.InputImage[c.Southwest] + this.InputImage[c.South] + this.InputImage[c.Southeast]) - (this.InputImage[c.Northwest] + this.InputImage[c.North] + this.InputImage[c.Northeast])) / 3;
        this.deltaYImage[c] = Dy;
        this.Result[c] = (int) Math.Round(Math.Sqrt(Dx * Dx + Dy * Dy));
      } while (c.MoveNext());
      OnIntermediateResult(new AlgorithmIntermediateResultEventArgs(this.deltaXImage, "Gradiente lungo asse x"));
      OnIntermediateResult(new AlgorithmIntermediateResultEventArgs(this.deltaYImage, "Gradiente lungo asse y"));
    }
  }


  [AlgorithmInfo("Trasformata distanza", Category = "FEI")]
  public class TrasformataDistanza : TopologyOperation<Image<int>>
  {
    public TrasformataDistanza()
    {
    }

    public TrasformataDistanza(Image<byte> inputImage, byte foreground, MetricType metric)
      : base(inputImage, foreground)
    {
      Metric = metric;
    }

    public override void Run()
    {
      this.Result = new Image<int>(this.InputImage.Width, this.InputImage.Height, 0);
      ImageCursor c = new ImageCursor(this.InputImage, 1);
      if (Metric == MetricType.CityBlock)
      {
        do
        {
          if (InputImage[c] == Foreground)
          {
            this.Result[c] = Math.Min(this.Result[c.West], this.Result[c.North]) + 1;
          }
        } while (c.MoveNext());
        do
        {
          if (InputImage[c] == Foreground)
          {
            this.Result[c] = Math.Min(this.Result[c.East] + 1, Math.Min(this.Result[c.South] + 1, this.Result[c]));
          }
        } while (c.MovePrevious());
      }
      else
      {
        do
        {
          if (InputImage[c] == Foreground)
          {
            this.Result[c] = Math.Min(this.Result[c.West], Math.Min(this.Result[c.North], Math.Min(this.Result[c.Northwest], this.Result[c.Northeast]))) + 1;
          }
        } while (c.MoveNext());
        do
        {
          if (InputImage[c] == Foreground)
          {
            this.Result[c] = Math.Min(this.Result[c.East] + 1, Math.Min(this.Result[c.South] + 1, Math.Min(Math.Min(this.Result[c.Southeast] + 1, this.Result[c.Southwest] + 1), this.Result[c])));
          }
        } while (c.MovePrevious());
      }
    }

    [AlgorithmParameter]
    public MetricType Metric { get; set; }
  }


}
