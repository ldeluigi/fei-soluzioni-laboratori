﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{

  [AlgorithmInfo("Morfologia: Dilatazione", Category = "FEI")]
  public class Dilatazione : MorphologyOperation
  {
    public Dilatazione(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
      : base(inputImage, structuringElement, foreground)
    {
    }

    public Dilatazione(Image<byte> inputImage, Image<byte> structuringElement)
      : base(inputImage, structuringElement)
    {
    }

    public Dilatazione()
    {
    }
    public override void Run()
    {
      Result = new Image<byte>(InputImage.Width, InputImage.Height, (255 - Foreground).ClipToByte());

      int[] elementOffsets = MorphologyStructuringElement.CreateOffsets(StructuringElement, InputImage, true);

      ImageCursor c = new ImageCursor(StructuringElement.Width / 2,
        StructuringElement.Height / 2,
        InputImage.Width - 1 - StructuringElement.Width / 2,
        InputImage.Height - 1 - StructuringElement.Height / 2,
        InputImage);

      do
      {
        foreach (int offset in elementOffsets)
        {
          if (InputImage[c + offset] == Foreground)
          {
            Result[c] = Foreground;
            break;
          }
        }
      } while (c.MoveNext());
    }
  }

  [AlgorithmInfo("Morfologia: Erosione", Category = "FEI")]
  public class Erosione : MorphologyOperation
  {
    public Erosione(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
      : base(inputImage, structuringElement, foreground)
    {
    }

    public Erosione(Image<byte> inputImage, Image<byte> structuringElement)
      : base(inputImage, structuringElement)
    {
    }

    public Erosione()
    {
    }

    public override void Run()
    {
      Result = new Image<byte>(InputImage.Width, InputImage.Height, (255 - Foreground).ClipToByte());

      int[] elementOffsets = MorphologyStructuringElement.CreateOffsets(StructuringElement, InputImage, false);

      ImageCursor c = new ImageCursor(StructuringElement.Width / 2,
        StructuringElement.Height / 2,
        InputImage.Width - 1 - StructuringElement.Width / 2,
        InputImage.Height - 1 - StructuringElement.Height / 2,
        InputImage);

      do
      {
        if (Array.TrueForAll(elementOffsets, offset => InputImage[c + offset] == Foreground))
        {
            Result[c] = Foreground;
        }
      } while (c.MoveNext());
    }
  }

  [AlgorithmInfo("Morfologia: Apertura", Category = "FEI")]
  public class Apertura : MorphologyOperation
  {
    public Apertura(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
      : base(inputImage, structuringElement, foreground)
    {
    }

    public Apertura(Image<byte> inputImage, Image<byte> structuringElement)
      : base(inputImage, structuringElement)
    {
    }

    public Apertura()
    {
    }

    public override void Run()
    {
      Erosione e = new Erosione(InputImage, StructuringElement, Foreground);

      Dilatazione d = new Dilatazione(e.Execute(), StructuringElement, Foreground);

      Result = d.Execute();
    }
  }

  [AlgorithmInfo("Morfologia: Chiusura", Category = "FEI")]
  public class Chiusura : MorphologyOperation
  {
    public Chiusura(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
      : base(inputImage, structuringElement, foreground)
    {
    }

    public Chiusura(Image<byte> inputImage, Image<byte> structuringElement)
      : base(inputImage, structuringElement)
    {
    }

    public Chiusura()
    {
    }

    public override void Run()
    {
      Dilatazione d = new Dilatazione(InputImage, StructuringElement, Foreground);

      Erosione e = new Erosione(d.Execute(), StructuringElement, Foreground);

      Result = e.Execute();
    }
  }

  [AlgorithmInfo("Estrazione Bordo con Morfologia", Category = "FEI")]
  public class EstrazioneBordo : MorphologyOperation
  {
    public EstrazioneBordo(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
      : base(inputImage, structuringElement, foreground)
    {
    }

    public EstrazioneBordo(Image<byte> inputImage, Image<byte> structuringElement)
      : base(inputImage, structuringElement)
    {
    }

    public EstrazioneBordo()
    {
    }

    public override void Run()
    {

      Erosione e = new Erosione(InputImage, StructuringElement, Foreground);

      var a = new ImageArithmetic(InputImage, e.Execute(), ImageArithmeticOperation.Subtract);
      Result = a.Execute();
    }
  }




    [AlgorithmInfo("Hit or Miss transform", Category = "FEI")]
    public class HitOrMissTransform : MorphologyOperation
    {
        [AlgorithmParameter]
        public Image<byte> StructuringElementBackground { get; set; }
        public HitOrMissTransform(Image<byte> inputImage, Image<byte> structuringElement, byte foreground)
          : base(inputImage, structuringElement, foreground)
        {
        }

        public HitOrMissTransform(Image<byte> inputImage, Image<byte> structuringElement)
          : base(inputImage, structuringElement)
        {
        }

        public HitOrMissTransform()
        {
        }

        public override void Run()
        {
            if (new ImageArithmetic(StructuringElement, StructuringElementBackground, ImageArithmeticOperation.And).Execute().Any(p => p != 0))
            {
                throw new ArgumentException("Structuring elements intersection must be null");
            }
            Erosione e = new Erosione(InputImage, StructuringElement, Foreground);
            Erosione e2 = new Erosione(new Image<byte>(InputImage.Width, InputImage.Height, InputImage.Select(p => (255 - p).ClipToByte()).ToArray()), StructuringElementBackground, Foreground);
            OnIntermediateResult(new AlgorithmIntermediateResultEventArgs(e.Execute(), "Prima erosione"));
            OnIntermediateResult(new AlgorithmIntermediateResultEventArgs(e2.Execute(), "Seconda erosione"));
            this.Result = new ImageArithmetic(e.Execute(), e2.Execute(), ImageArithmeticOperation.And).Execute();
        }
    }
}
