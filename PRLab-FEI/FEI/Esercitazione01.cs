﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;

namespace PRLab.FEI
{

  [AlgorithmInfo("Negativo Grayscale", Category = "FEI")]
  public class NegativeImage : ImageOperation<Image<byte>,Image<byte>>
  {
    public override void Run()
    {
      Result = new Image<byte>(InputImage.Width, InputImage.Height, InputImage.Select(p => (255 - p).ClipToByte()).ToArray());
    }
  }

    [AlgorithmInfo("Negativo Grayscale RGB", Category = "FEI")]
    public class NegativeImageRgb : ImageOperation<RgbImage<byte>, RgbImage<byte>>
    {
        public override void Run()
        {
            Result = new RgbImage<byte>(new Image<byte>(InputImage.Width, InputImage.Height, InputImage.RedChannel.Select(p => (255 - p).ClipToByte()).ToArray()),
                                        new Image<byte>(InputImage.Width, InputImage.Height, InputImage.GreenChannel.Select(p => (255 - p).ClipToByte()).ToArray()),
                                        new Image<byte>(InputImage.Width, InputImage.Height, InputImage.BlueChannel.Select(p => (255 - p).ClipToByte()).ToArray()));
        }
    }

    [AlgorithmInfo("Modifica Luminosità", Category = "FEI")]
    public class EditBrightness : ImageOperation<Image<byte>, Image<byte>>
    {
        [AlgorithmParameter] public int variation { get; set; }
        public override void Run()
        {
            if (this.variation < -100 || this.variation > 100)
            {
                throw new ArgumentOutOfRangeException("variation è compresa tra -100 e 100");
            }
            else
            {
                Result = new Image<byte>(InputImage.Width, InputImage.Height, InputImage.Select(p => (p + 255 * this.variation / 100.0).RoundAndClipToByte()).ToArray());
            }
        }
    }

    [AlgorithmInfo("Genera pseudocolori da grayscale", Category = "FEI")]
    public class PseudocoloringGrayscaleImage : RgbLookupTableTransform<byte>
    {
        public PseudocoloringGrayscaleImage(Image<byte> input) : base(input, LookupTables.Spectrum)
        {

        }
    }

}
