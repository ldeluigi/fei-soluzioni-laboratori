﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{


    [AlgorithmInfo("Etichettatura delle componenti connesse", Category = "FEI")]
    public class EtichettaturaComponentiConnesse : TopologyOperation<ConnectedComponentImage>
    {
        public EtichettaturaComponentiConnesse(Image<byte> inputImage, byte foreground, MetricType metric)
          : base(inputImage, foreground)
        {
            Metric = metric;
        }

        public EtichettaturaComponentiConnesse(Image<byte> inputImage)
          : this(inputImage, 255, MetricType.Chessboard)
        {
        }

        public EtichettaturaComponentiConnesse()
        {
            Metric = MetricType.Chessboard;
        }

        public override void Run()
        {
            Result = new ConnectedComponentImage(InputImage.Width, InputImage.Height, -1);
            var cursor = new ImageCursor(InputImage, 1); // per semplicità ignora i bordi (1 pixel)            
            int[] neighborLabels = new int[Metric == MetricType.CityBlock ? 2 : 4];
            int nextLabel = 0;
            var equivalences = new DisjointSets(InputImage.PixelCount);
            do
            { // prima scansione
                if (InputImage[cursor] == Foreground)
                {
                    int labelCount = 0;
                    if (Result[cursor.West] >= 0) neighborLabels[labelCount++] = Result[cursor.West];
                    if (Result[cursor.North] >= 0) neighborLabels[labelCount++] = Result[cursor.North];
                    if (Metric == MetricType.Chessboard)
                    {   // anche le diagonali
                        if (Result[cursor.Northwest] >= 0) neighborLabels[labelCount++] = Result[cursor.Northwest];
                        if (Result[cursor.Northeast] >= 0) neighborLabels[labelCount++] = Result[cursor.Northeast];
                    }
                    if (labelCount == 0)
                    {
                        equivalences.MakeSet(nextLabel); // crea un nuovo set
                        Result[cursor] = nextLabel++; // le etichette iniziano da 0
                    }
                    else
                    {
                        int l = Result[cursor] = neighborLabels[0]; // seleziona la prima
                        for (int i = 1; i < labelCount; i++) // equivalenze
                            if (neighborLabels[i] != l)
                                equivalences.MakeUnion(neighborLabels[i], l); // le rende equivalenti
                    }
                }
            } while (cursor.MoveNext());
            int totalLabels;
            int[] corresp = equivalences.Renumber(nextLabel, out totalLabels);
            // seconda e ultima scansione
            cursor.Restart();
            do
            {
                int l = Result[cursor];
                if (l >= 0)
                {
                    Result[cursor] = corresp[l];
                }
            } while (cursor.MoveNext());
            Result.ComponentCount = totalLabels;
        }

        [AlgorithmParameter]
        [DefaultValue(MetricType.Chessboard)]
        public MetricType Metric { get; set; }


    }

    [AlgorithmInfo("Informazioni componenti connesse", Category = "FEI")]
    public class InformazioniComponentiConnesse : Algorithm
    {
        [AlgorithmInput]
        public Image<byte> Input { get; set; }
        [AlgorithmParameter]
        [DefaultValue(255)]
        public byte Foreground { get; set; } = 255;
        [AlgorithmParameter]
        [DefaultValue(MetricType.Chessboard)]
        public MetricType Metric { get; set; }
        [AlgorithmOutput]
        public int NumeroComponentiConnesse { get; set; }
        [AlgorithmOutput]
        public int AreaMinima { get; set; }
        [AlgorithmOutput]
        public int AreaMassima { get; set; }
        [AlgorithmOutput]
        public double AreaMedia { get; set; }
        [AlgorithmOutput]
        public double LunghezzaMediaPerimetro { get; set; }

        public override void Run()
        {
            ConnectedComponentImage cci = new EtichettaturaComponentiConnesse(this.Input, this.Foreground, this.Metric).Execute();
            this.NumeroComponentiConnesse = cci.ComponentCount;
            ImageCursor c = new ImageCursor(cci);
            var result = iterateImage(cci).
                Where(p => cci[p] >= 0)
                .GroupBy(p => cci[p])
                .Select(g => new
                {
                    Area = g.Count(),
                    Etichetta = cci[g.Key],
                    Lunghezza = g.Select(p => p.X == 0 || p.X == cci.Height-1 || p.Y == 0 || p.Y == cci.Width-1 ||
                                            cci[p.North] == cci[g.Key] ||
                                            cci[p.South] == cci[g.Key] ||
                                            cci[p.East] == cci[g.Key] ||
                                            cci[p.West] == cci[g.Key] ?
                                            1 : 0).Sum()
                })
                .Aggregate(new
                {
                    AMin = int.MaxValue,
                    AMax = 0,
                    ASum = 0,
                    LSum = 0
                },
                (accumulator, o) => new
                {
                    AMin = Math.Min(o.Area, accumulator.AMin),
                    AMax = Math.Max(o.Area, accumulator.AMax),
                    ASum = o.Area + accumulator.ASum,
                    LSum = o.Lunghezza + accumulator.LSum
                });
            this.AreaMassima = result.AMax;
            this.AreaMinima = result.AMin;
            this.AreaMedia = result.ASum / this.NumeroComponentiConnesse;
            this.LunghezzaMediaPerimetro = result.LSum / this.NumeroComponentiConnesse;
        }
        private IEnumerable<ImageCursor> iterateImage(ImageBase image)
        {
            ImageCursor c = new ImageCursor(image);
            do
            {
                yield return (ImageCursor) c.Clone();
            } while (c.MoveNext());
        }
    }

    [AlgorithmInfo("Elimina Piccole Componenti", Category = "FEI")]
    public class EliminaPiccoleComponenti : TopologyOperation<Image<byte>>
    {
        [AlgorithmParameter]
        [DefaultValue(0)]
        public int AreaMinima { get; set; } = 0;
        public EliminaPiccoleComponenti(Image<byte> inputImage, byte foreground, MetricType metric)
          : base(inputImage, foreground)
        {
            Metric = metric;
        }

        public EliminaPiccoleComponenti(Image<byte> inputImage)
          : this(inputImage, 255, MetricType.Chessboard)
        {
        }

        public EliminaPiccoleComponenti()
        {
            Metric = MetricType.Chessboard;
        }

        public override void Run()
        {
            ConnectedComponentImage cci = new EtichettaturaComponentiConnesse(this.InputImage, this.Foreground, this.Metric).Execute();
            int[] remove = cci.GroupBy(c => c).Where(g => g.Count() < this.AreaMinima).Select(g => g.Key).ToArray();
            this.Result = this.InputImage.Clone();
            ImageCursor cursor = new ImageCursor(this.Result);
            do
            {
                if (remove.Contains(cci[cursor]))
                {
                    this.Result[cursor] = 0;
                }
            } while (cursor.MoveNext());
        }

        [AlgorithmParameter]
        [DefaultValue(MetricType.Chessboard)]
        public MetricType Metric { get; set; }


    }
}