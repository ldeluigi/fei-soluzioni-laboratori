﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI
{

    [AlgorithmInfo("Calcolo Istogramma", Category = "FEI")]
    public class HistogramBuilder : ImageOperation<Image<byte>, Histogram>
    {
        [AlgorithmParameter]
        [DefaultValue(false)]
        public bool Sqrt { get; set; }

        [AlgorithmParameter]
        [DefaultValue(0)]
        public int SmoothWindowSize { get; set; }

        public HistogramBuilder(Image<byte> inputImage)
        {
            InputImage = inputImage;
        }

        public HistogramBuilder(Image<byte> inputImage, bool sqrt, int smoothWindowSize)
        {
            InputImage = inputImage;
            Sqrt = sqrt;
            SmoothWindowSize = smoothWindowSize;
        }

        public override void Run()
        {
            Result = new Histogram();
            foreach (byte b in InputImage)
            {
                Result[b]++;
            }
            if (Sqrt)
            {
                Result = new Histogram(Result.Select(e => (int)Math.Round(Math.Sqrt(e))).ToArray());
            }
            if (SmoothWindowSize > 0)
            {
                Result = new Histogram(Result.Select((e, i) =>
                {
                    int start = Math.Max(0, i - SmoothWindowSize);
                    int count = Math.Min(2 * SmoothWindowSize + 1 + Math.Min(0, i - SmoothWindowSize), Result.Count() - start);
                    return (int)Math.Round(Result.ToList().GetRange(start, count).Average());
                }).ToArray());
            }
        }
    }

    [AlgorithmInfo("Binarizzazione Immagine", Category = "FEI")]
    public class CustomImageBinarization : ImageOperation<Image<byte>, Image<byte>>
    {
        [AlgorithmParameter]
        [DefaultValue(128)]
        public byte Threshold { get; set; }

        public CustomImageBinarization(Image<byte> inputImage) : this(inputImage, 128)
        {
        }

        public CustomImageBinarization(Image<byte> inputImage, byte threshold)
        {
            this.Threshold = threshold;
            this.InputImage = inputImage;
        }

        public override void Run()
        {
            Result = new Image<byte>(InputImage.Width, InputImage.Height, InputImage.Select(e => e > Threshold ? Byte.MaxValue : Byte.MinValue).ToArray());
        }
    }

    [AlgorithmInfo("Split Immagine", Category = "FEI")]
    public class ChannelSplit : Algorithm
    {
        [AlgorithmOutput] public Image<byte> R { get; private set; }
        [AlgorithmOutput] public Image<byte> G { get; private set; }
        [AlgorithmOutput] public Image<byte> B { get; private set; }
        [AlgorithmInput] public RgbImage<byte> Input { get; set; }

        public override void Run()
        {
            if (Input != null)
            {
                R = Input.RedChannel.Clone();
                G = Input.GreenChannel.Clone();
                B = Input.BlueChannel.Clone();
            }
        }
    }

    [AlgorithmInfo("Reverse Split Image", Category = "FEI")]
    public class ChannelReverseSplit : Algorithm
    {
        [AlgorithmInput] public Image<byte> R { get; set; }
        [AlgorithmInput] public Image<byte> G { get; set; }
        [AlgorithmInput] public Image<byte> B { get; set; }
        [AlgorithmOutput] public RgbImage<byte> Output { get; private set; }

        public override void Run()
        {
            if (R != null && G != null && B != null)
            {
                Output = new RgbImage<byte>(R, G, B);
            }
        }
    }

    [AlgorithmInfo("Adjust Brighness", Category = "FEI")]
    public class ChangeBrightness : ImageOperation<RgbImage<byte>, RgbImage<byte>>
    {
        [AlgorithmParameter] public int Percentage { get; set; }
        public override void Run()
        {
            if (InputImage != null)
            {
                Result = new RgbImage<byte>(new BrightnessAdjustment(InputImage.RedChannel, Percentage).Execute(), new BrightnessAdjustment(InputImage.GreenChannel, Percentage).Execute(), new BrightnessAdjustment(InputImage.BlueChannel, Percentage).Execute());
            }
        }
    }

    [AlgorithmInfo("HSL Split", Category = "FEI")]
    public class HSLSplit : Algorithm
    {
        [AlgorithmOutput] public Image<byte> H { get; private set; }
        [AlgorithmOutput] public Image<byte> S { get; private set; }
        [AlgorithmOutput] public Image<byte> L { get; private set; }
        [AlgorithmInput] public RgbImage<byte> Input { get; set; }
        public override void Run()
        {
            var i = Input.ToByteHslImage();
            H = i.HueChannel.Clone();
            S = i.SaturationChannel.Clone();
            L = i.LightnessChannel.Clone();
        }
    }

    [AlgorithmInfo("Reverse HSL Split", Category = "FEI")]
    public class ReverseHSLSplit : Algorithm
    {
        [AlgorithmInput] public Image<byte> H { get; set; }
        [AlgorithmInput] public Image<byte> S { get; set; }
        [AlgorithmInput] public Image<byte> L { get; set; }
        [AlgorithmOutput] public RgbImage<byte> Output { get; private set; }

        public override void Run()
        {
            if (H != null && S != null && L != null)
            {
                Output = new HslImage<byte>(H, S, L).ToByteRgbImage();
            }
        }
    }

    [AlgorithmInfo("Fake Colorizer", Category = "FEI")]
    public class FakeCustomColor: ImageOperation<Image<byte>, RgbImage<byte>>
    {
        [AlgorithmParameter]
        public byte Hue { get; set; }

        [AlgorithmParameter]
        public byte Saturation { get; set; }
        public override void Run()
        {
            Result = new HslImage<byte>(new Image<byte>(InputImage.Width, InputImage.Height, Hue), new Image<byte>(InputImage.Width, InputImage.Height, Saturation), InputImage).ToByteRgbImage();
        }
    }
}


