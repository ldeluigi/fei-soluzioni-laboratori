﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;
using BioLab.GUI.UserControls;

namespace PRLab.FEI
{

  [AlgorithmInfo("Convoluzione (da immagine byte a immagine int)", Category = "FEI")]
  [CustomAlgorithmPreviewParameterControl(typeof(SimpleConvolutionParameterControl))]
  public class ConvoluzioneByteInt : Convolution<byte, int, ConvolutionFilter<int>>
  {
    public override void Run()
    {
      Result = new Image<int>(InputImage.Width, InputImage.Height, 0);
      int[] offsets;
      int[] values = Filter.CalculateConvolutionValuesAndOffsets(InputImage.Width, out offsets);
      ImageCursor c = new ImageCursor(InputImage, Filter.Size / 2);
      do
      {
        Result[c] = offsets.Select((offset, i) => InputImage[c + offset] * values[i]).Sum() / Filter.Denominator;
      } while (c.MoveNext());
    }
  }

  [AlgorithmInfo("Smoothing", Category = "FEI")]
  public class Smoothing : ImageOperation<Image<byte>, Image<byte>>
  {
    [AlgorithmParameter]
    [DefaultValue(3)]
    public int FilterSize { get; set; }
    public override void Run()
    {
      ConvoluzioneByteInt c = new ConvoluzioneByteInt();
      c.InputImage = InputImage;
      c.Filter = new ConvolutionFilter<int>(Enumerable.Repeat(1, FilterSize * FilterSize).ToArray(), FilterSize * FilterSize);
      Result = c.Execute().ToByteImage();
    }
  }


  [AlgorithmInfo("Sharpening", Category = "FEI")]
  public class Sharpening : ImageOperation<Image<byte>, Image<byte>>
  {
    private double _fs = 0;
    [AlgorithmParameter]
    [DefaultValue(1.0)]
    public double FilterWeight
    {
      get
      {
        return this._fs;
      }
      set
      {
        if (value < 0 || value > 1)
          throw new ArgumentOutOfRangeException("Inserire un valore fra 0 and 1.0.");
        this._fs = value;
      }
    }
    public override void Run()
    {
      ConvoluzioneByteInt c = new ConvoluzioneByteInt();
      c.InputImage = InputImage;
      int[] f = Enumerable.Repeat(-1, 9).ToArray();
      f[4] = 8;
      c.Filter = new ConvolutionFilter<int>(f, 9);
      Image<int> im = c.Execute();
      Result = new Image<byte>(InputImage.Width, InputImage.Height, InputImage.Select((p, i) => (p + im[i] * FilterWeight).RoundAndClipToByte()).ToArray());
    }
  }
}
