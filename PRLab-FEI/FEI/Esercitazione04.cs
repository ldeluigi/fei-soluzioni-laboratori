﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI
{

  public abstract class TrasformazioneAffine<TImage> : ImageOperation<TImage, TImage>
    where TImage : ImageBase
  {
    protected TrasformazioneAffine()
    {
    }

    protected TrasformazioneAffine(TImage inputImage, double translationX, double translationY, double rotationDegrees, double scaleFactorX, double scaleFactorY, int resultWidth, int resultHeight)
      : base(inputImage)
    {
      TranslationX = translationX;
      TranslationY = translationY;
      RotationDegrees = rotationDegrees;
      ScaleFactorX = scaleFactorX;
      ScaleFactorY = scaleFactorY;
      ResultWidth = resultWidth;
      ResultHeight = resultHeight;
    }

    protected TrasformazioneAffine(TImage inputImage, double translationX, double translationY, double rotationDegrees)
      : this(inputImage, translationX, translationY, rotationDegrees, 1, 1, inputImage.Width, inputImage.Height)
    {
    }

    protected TrasformazioneAffine(TImage inputImage, double scaleFactor)
      : this(inputImage, 0, 0, 0, scaleFactor, scaleFactor, 0, 0)
    {
    }

    private double translationX;
    private double rotationDegrees;
    private double translationY;
    private double cx = 0.5;
    private double cy = 0.5;
    private double scaleFactorX = 1.0;
    private double scaleFactorY = 1.0;

    [AlgorithmParameter]
    [DefaultValue(0)]
    public double TranslationX { get { return translationX; } set { translationX = value; } }

    [AlgorithmParameter]
    [DefaultValue(0)]
    public double TranslationY { get { return translationY; } set { translationY = value; } }

    [AlgorithmParameter]
    [DefaultValue(0)]
    public double RotationDegrees { get { return rotationDegrees; } set { rotationDegrees = value; } }

    [AlgorithmParameter]
    [DefaultValue(0.5)]
    public double RotationCenterX { get { return cx; } set { cx = value; } }

    [AlgorithmParameter]
    [DefaultValue(0.5)]
    public double RotationCenterY { get { return cy; } set { cy = value; } }

    [AlgorithmParameter]
    [DefaultValue(1)]
    public double ScaleFactorX { get { return scaleFactorX; } set { scaleFactorX = value; } }

    [AlgorithmParameter]
    [DefaultValue(1)]
    public double ScaleFactorY { get { return scaleFactorY; } set { scaleFactorY = value; } }

    [AlgorithmParameter]
    [DefaultValue(0)]
    public int ResultWidth { get; set; }

    [AlgorithmParameter]
    [DefaultValue(0)]
    public int ResultHeight { get; set; }
  }

  [AlgorithmInfo("Trasformazione affine (grayscale)", Category = "FEI")]
  public class TrasformazioneAffineGrayscale : TrasformazioneAffine<Image<byte>>
  {
    [AlgorithmParameter]
    [DefaultValue(0)]
    public byte Background { get; set; }

    public TrasformazioneAffineGrayscale(Image<byte> inputImage, double translationX, double translationY, double rotationDegrees, double scaleFactorX, double scaleFactorY, byte background, int resultWidth, int resultHeight)
      : base(inputImage, translationX, translationY, rotationDegrees, scaleFactorX, scaleFactorY, resultWidth, resultHeight)
    {
      Background = background;
    }

    public TrasformazioneAffineGrayscale(Image<byte> inputImage, double translationX, double translationY, double rotationDegrees, byte background)
      : base(inputImage, translationX, translationY, rotationDegrees)
    {
      Background = background;
    }

    public TrasformazioneAffineGrayscale(Image<byte> inputImage, double scaleFactor, byte background)
      : base(inputImage, scaleFactor)
    {
      Background = background;
    }

    public TrasformazioneAffineGrayscale()
    {
    }
    public double ConvertToRadians(double angle)
    {
      return (Math.PI / 180) * angle;
    }
 
    public override void Run()
    {
      double rads = ConvertToRadians(RotationDegrees);
      double[][] matMul = { new double[]{ Math.Cos(-rads) / ScaleFactorX, Math.Sin(-rads) / ScaleFactorX }, new double[] { - Math.Sin(-rads) / ScaleFactorY, Math.Cos(-rads) / ScaleFactorY } };
      Result = new Image<byte>(ResultWidth <= 0 ? InputImage.Width : ResultWidth, ResultHeight <= 0 ? InputImage.Height : ResultHeight);
      for (int i = 0; i < Result.Height; i++)
      {
        for (int j = 0; j < Result.Width; j++)
        {
          double[] coords = ReverseMap(j, i, matMul, new double[2] { TranslationX , TranslationY });
          Result[i, j] = LagrangeInterpolate(coords[0], coords[1], InputImage);
        }
      }
    }

    private double[] ReverseMap(int newX, int newY, double[][] matMul, double[] trasVet)
    {
      double[] tras = { newX - trasVet[0], newY - trasVet[1] };
      return new double[2] { matMul[0][0] * tras[0] + matMul[0][1] * tras[1], matMul[1][0] * tras[0] + matMul[1][1] * tras[1] };
    }
 
    private byte LagrangeInterpolate(double oldX, double oldY, Image<byte> input)
    {
      if (oldY < 0 || oldX < 0 || oldY >= input.Height || oldX >= input.Width) return Background;
      if (oldY >= input.Height - 1 || oldX >= input.Width - 1) return input[(int)Math.Floor(oldY), (int)Math.Floor(oldX)];
      int xl = (int) Math.Floor(oldX);
      int yl = (int) Math.Floor(oldY);
      double wa = ( xl + 1 - oldX ) * ( yl + 1 - oldY );
      double wb = ( oldX - xl ) * ( yl + 1 - oldY );
      double wc = ( xl + 1 - oldX ) * ( oldY - yl );
      double wd = ( oldX - xl ) * ( oldY - yl );
      return (input[yl, xl] * wa + input[yl + 1, xl] * wc + input[yl, xl + 1] * wb + input[yl + 1, xl + 1] * wd).RoundAndClipToByte();
    }
  }

  [AlgorithmInfo("Trasformazione affine (rgb)", Category = "FEI")]
  public class TrasformazioneAffineRgb : TrasformazioneAffine<RgbImage<byte>>
  {
    [AlgorithmParameter]
    public RgbPixel<byte> Background { get; set; }

    public TrasformazioneAffineRgb(RgbImage<byte> inputImage, double translationX, double translationY, double rotationDegrees, double scaleFactorX, double scaleFactorY, RgbPixel<byte> background, int resultWidth, int resultHeight)
      : base(inputImage, translationX, translationY, rotationDegrees, scaleFactorX, scaleFactorY, resultWidth, resultHeight)
    {
      Background = background;
    }

    public TrasformazioneAffineRgb(RgbImage<byte> inputImage, double translationX, double translationY, double rotationDegrees, RgbPixel<byte> background)
      : base(inputImage, translationX, translationY, rotationDegrees)
    {
      Background = background;
    }

    public TrasformazioneAffineRgb(RgbImage<byte> inputImage, double scaleFactor, RgbPixel<byte> background)
      : base(inputImage, scaleFactor)
    {
      Background = background;
    }

    public TrasformazioneAffineRgb()
    {
    }

    public override void Run()
    {
      var r = new TrasformazioneAffineGrayscale(InputImage.RedChannel, TranslationX, TranslationY, RotationDegrees, ScaleFactorX, ScaleFactorY, Background.Red, ResultWidth, ResultHeight);
      var g = new TrasformazioneAffineGrayscale(InputImage.GreenChannel, TranslationX, TranslationY, RotationDegrees, ScaleFactorX, ScaleFactorY, Background.Green, ResultWidth, ResultHeight);
      var b = new TrasformazioneAffineGrayscale(InputImage.BlueChannel, TranslationX, TranslationY, RotationDegrees, ScaleFactorX, ScaleFactorY, Background.Blue, ResultWidth, ResultHeight);
      Result = new RgbImage<byte>(r.Execute(), g.Execute(), b.Execute());
    }
  }

}
