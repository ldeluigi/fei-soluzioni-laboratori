﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BioLab.Common;
using BioLab.ImageProcessing;
using System.Drawing;
using BioLab.GUI.Forms;
using System.Windows.Forms;
using BioLab.ImageProcessing.Topology;
using BioLab.DataStructures;
using System.ComponentModel;

namespace PRLab.FEI
{

  [AlgorithmInfo("Stretch del contrasto", Category = "FEI")]
  public class ContrastStretch : ImageOperation<Image<byte>, Image<byte>>
  {
    private int stretchDiscardPerc;

    [AlgorithmParameter]
    [DefaultValue(0)]
    public int StretchDiscardPercentage
    {
      get { return stretchDiscardPerc; }
      set
      {
        if (value < 0 || value > 100)
          throw new ArgumentOutOfRangeException("Inserire un valore fra 0 and 100.");
        stretchDiscardPerc = value;
      }
    }

    public ContrastStretch()
    {
    }

    public ContrastStretch(Image<byte> inputImage)
      : base(inputImage)
    {
      StretchDiscardPercentage = 0;
    }

    public ContrastStretch(Image<byte> inputImage, int stretchDiscard)
      : base(inputImage)
    {
      StretchDiscardPercentage = stretchDiscard;
    }

    public override void Run()
    {
            HistogramBuilder hb = new HistogramBuilder(InputImage);
            Histogram h = hb.Execute();
            int tot = InputImage.PixelCount;
            int rem = (int) Math.Round(tot * StretchDiscardPercentage / 200.0);
            int j = 0;
            for (int i = rem; i >= 0;)
            {
                i -= h[j];
                j++;
            }
            int w = h.Count() - 1;
            for (int i = rem; i >= 0;)
            {
                i -= h[w];
                w--;
            }
            Result = new Image<byte>(InputImage.Width, InputImage.Height, InputImage.Select(p => p >= j ? (p <= w ? p : Byte.MaxValue) : Byte.MinValue).ToArray());
            int diff = w - j;
            if (diff > 0)
            {
                var op = new LookupTableTransform<byte>(Result, p => (Byte.MaxValue * (p - j) / diff).ClipToByte());
                Result = op.Execute();
            }
        }
  }


  [AlgorithmInfo("Equalizzazione istogramma", Category = "FEI")]
  public class HistogramEqualization : ImageOperation<Image<byte>, Image<byte>>
  {
    public HistogramEqualization()
    {
    }

    public HistogramEqualization(Image<byte> inputImage)
      : base(inputImage)
    {
    }

        public override void Run()
        {
            HistogramBuilder hb = new HistogramBuilder(InputImage);
            Histogram h = hb.Execute();
            int w = 0;
            h = new Histogram(h.Select(e => w += e).ToArray());
            var op = new LookupTableTransform<byte>(InputImage, p => (Byte.MaxValue * h[p] / InputImage.PixelCount).ClipToByte());
            Result = op.Execute();
        }
  }

  [AlgorithmInfo("Operazione aritmetica", Category = "FEI")]
  public class ImageArithmetic : ImageOperation<Image<byte>, Image<byte>, Image<byte>>
  {
    [AlgorithmParameter]
    [DefaultValue(defaultOperation)]
    public ImageArithmeticOperation Operation { get; set; }
    const ImageArithmeticOperation defaultOperation = ImageArithmeticOperation.Difference;

    public ImageArithmetic()
    {
    }

    public ImageArithmetic(Image<byte> image1, Image<byte> image2, ImageArithmeticOperation operation)
      : base(image1, image2)
    {
      Operation = operation;
    }

    public ImageArithmetic(Image<byte> image1, Image<byte> image2)
      : this(image1, image2, defaultOperation)
    {
    }

    public override void Run()
    {
            if (InputImage1.Width != InputImage2.Width || InputImage1.Height != InputImage2.Height)
            {
                throw new ArgumentException();
            }
            Func<int, int, int> op = (a, b) => 0;
            switch(Operation)
            {
                case ImageArithmeticOperation.Add:
                    op = (a, b) => a + b;
                    break;
                case ImageArithmeticOperation.And:
                    op = (a, b) => a & b;
                    break;
                case ImageArithmeticOperation.Average:
                    op = (a, b) => (int) Math.Round((a + b) / 2.0);
                    break;
                case ImageArithmeticOperation.Darkest:
                    op = (a, b) => a > b ? b : a;
                    break;
                case ImageArithmeticOperation.Difference:
                    op = (a, b) => Math.Abs(a - b);
                    break;
                case ImageArithmeticOperation.Lightest:
                    op = (a, b) => a > b ? a : b;
                    break;
                case ImageArithmeticOperation.Or:
                    op = (a, b) => a | b;
                    break;
                case ImageArithmeticOperation.Subtract:
                    op = (a, b) => a - b;
                    break;
                case ImageArithmeticOperation.Xor:
                    op = (a, b) => a ^ b;
                    break;
            }
            Result = new Image<byte>(InputImage1.Width, InputImage1.Height, InputImage1.Select((p, i) => op(p, InputImage2[i]).ClipToByte()).ToArray());
    }
  }

    [AlgorithmInfo("Operazione aritmetica RGB", Category = "FEI")]
    public class ImageArithmeticRGB : ImageOperation<RgbImage<byte>, RgbImage<byte>, RgbImage<byte>>
    {
        [AlgorithmParameter]
        [DefaultValue(defaultOperation)]
        public ImageArithmeticOperation Operation { get; set; }
        const ImageArithmeticOperation defaultOperation = ImageArithmeticOperation.Difference;

        public ImageArithmeticRGB()
        {
        }

        public ImageArithmeticRGB(RgbImage<byte> image1, RgbImage<byte> image2, ImageArithmeticOperation operation)
          : base(image1, image2)
        {
            Operation = operation;
        }

        public ImageArithmeticRGB(RgbImage<byte> image1, RgbImage<byte> image2)
          : this(image1, image2, defaultOperation)
        {
        }

        public override void Run()
        {
            ImageOperation<Image<byte>, Image<byte>, Image<byte>> opR = new ImageArithmetic(InputImage1.RedChannel, InputImage2.RedChannel, Operation);
            ImageOperation<Image<byte>, Image<byte>, Image<byte>> opG = new ImageArithmetic(InputImage1.GreenChannel, InputImage2.GreenChannel, Operation);
            ImageOperation<Image<byte>, Image<byte>, Image<byte>> opB = new ImageArithmetic(InputImage1.BlueChannel, InputImage2.BlueChannel, Operation);
            Result = new RgbImage<byte>(opR.Execute(), opG.Execute(), opB.Execute());
        }
    }
}
